import matplotlib.pyplot as plt

import numpy as np
import eqsig
import sfsimodels as sm
import pysra

# import all_paths as ap
from example_opensees_site_response import op_funs

import os


CUR_DIR = os.path.dirname(os.path.abspath(__file__)) + "/"


def analyse(sp, in_sig, odepths, analysis):
    m = pysra.motion.TimeSeriesMotion(filename="", description="", time_step=in_sig.dt,
                                      accels=in_sig.values * 2 / 9.8)  # converted to g and times 2 (outcrop)

    oseries, profile, calc = op_funs.sra_w_pysra(sp, m, odepths, analysis=analysis)

    asigs = []
    for i, depth in enumerate(odepths):
        acc_signal = eqsig.AccSignal(oseries["ACCX_d%i" % i] * 9.8, m.time_step)
        asigs.append(acc_signal)

    return asigs


def create():
    scale = 0.01
    name = str(scale)
    name = name.replace(".", "p")
    accs, time = op_funs.get_acc_and_time(folder_path=CUR_DIR, name=name)
    in_acc, in_dt = op_funs.get_in_motion_and_dt()
    in_sig = eqsig.AccSignal(in_acc * scale, in_dt)

    mods = sm.load_json("ecp.json")
    sp = mods['soil_profile'][1]
    approx_t_site = 4 * sp.layer_height(1) / sp.layer(1).get_shear_vel(saturated=False)
    print("approx t_site: ", approx_t_site)

    dt = time[1] - time[0]
    surf_sig = eqsig.AccSignal(accs[:, -1], dt)

    sp.layer(1).xi = 0.05
    sp.layer(2).xi = 0.01

    eqlin_surf_sig = analyse(sp, in_sig, odepths=[0], analysis='linear')[0]

    bf, sps = plt.subplots(nrows=2, sharex=True)
    sps[0].loglog(in_sig.fa_frequencies, 2 * np.abs(in_sig.fa_spectrum), c='k', alpha=0.4)
    sps[0].loglog(in_sig.smooth_fa_frequencies, 2 * np.abs(in_sig.smooth_fa_spectrum), c='k', label="2 x Input")
    sps[0].loglog(surf_sig.fa_frequencies, np.abs(surf_sig.fa_spectrum), c='r', alpha=0.4)
    sps[0].loglog(surf_sig.smooth_fa_frequencies, np.abs(surf_sig.smooth_fa_spectrum), c='r', label='Surface')
    sps[0].loglog(eqlin_surf_sig.fa_frequencies, np.abs(eqlin_surf_sig.fa_spectrum), c='b', alpha=0.4, label='Linear - surface')
    sps[1].axvline(approx_t_site)
    sps[1].semilogx(surf_sig.smooth_fa_frequencies, np.abs(surf_sig.smooth_fa_spectrum) / np.abs(in_sig.smooth_fa_spectrum), c='r', label='Opensees')
    sps[1].semilogx(eqlin_surf_sig.smooth_fa_frequencies,
                    0.5 * np.abs(eqlin_surf_sig.smooth_fa_spectrum) / np.abs(in_sig.smooth_fa_spectrum), c='g',
                    label='Linear')
    sps[0].legend()
    plt.show()


if __name__ == '__main__':
    create()


