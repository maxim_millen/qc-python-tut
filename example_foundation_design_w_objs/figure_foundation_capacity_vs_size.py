import matplotlib.pyplot as plt
from bwplot import cbox
import engformat as ef
import numpy as np

import geofound as gf
import sfsimodels as sm

import all_paths as ap

import settings as ops


def create(save=0, show=0):

    soil = sm.Soil()
    soil.unit_dry_weight = 19e3  # N/m3
    soil.cohesion = 0.0
    soil.phi = 30.0

    sizes = np.linspace(0.5, 1.5, 10)
    m_caps = []
    v_caps = []
    target = 500e3  # N

    for i in range(len(sizes)):
        fd = sm.RaftFoundation()
        fd.width = sizes[i]
        fd.length = fd.width
        fd.depth = 0.3

        m_caps.append(gf.capacity_meyerhof_1963(soil, fd))
        v_caps.append(gf.capacity_vesics_1975(soil, fd))

    m_force = np.array(v_caps) * sizes ** 2
    v_force = np.array(m_caps) * sizes ** 2
    meyerhof_size = np.interp(target, m_force, sizes)
    vesics_size = np.interp(target, v_force, sizes)
    req_size = max([meyerhof_size, vesics_size])
    bf, subplot = plt.subplots(figsize=(ops.TWO_COL, 4))
    subplot.plot(sizes, v_force / 1e3, label='Vesics 1975')
    subplot.plot(sizes, m_force / 1e3, label='Meyerhof 1963')
    subplot.axhline(target / 1e3, c='r')
    subplot.axvline(req_size, c='r', ls='--', label='Min. size')
    subplot.legend()
    subplot.set_xlabel("Size [m]")
    subplot.set_ylabel("$N_{ult}$ [kN]")
    plt.show()

    ef.revamp_legend(subplot, loc="upper left", prop={'size': 9})

    bf.tight_layout()
    name = __file__.replace('.py', '')
    name = name.split("figure_")[-1]
    extension = ""

    if save:
        bf.savefig(ap.PUB_FIG_PATH + name + extension + ops.PUB_FIG_FILE_TYPE, dpi=ops.PUB_FIG_DPI)
        if ops.PUB_DOCUMENT_TYPE == "latex":
            para = ef.latex_for_figure(ap.FIG_FOLDER, name=ap.PUB_FIG_PATH + name + extension, ftype=ops.PUB_FIG_FILE_TYPE)
            print(para)
    if show:
        plt.show()


if __name__ == '__main__':
    create(save=0, show=1)
