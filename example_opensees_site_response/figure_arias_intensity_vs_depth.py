import os

import matplotlib.pyplot as plt
import numpy as np
import eqsig

from example_opensees_site_response import op_funs


CUR_DIR = os.path.dirname(os.path.abspath(__file__)) + "/"


def create():
    scale = 0.01
    name = str(scale)
    name = name.replace(".", "p")
    accs, time = op_funs.get_acc_and_time(folder_path=CUR_DIR, name=name)

    y_node_coords = np.loadtxt("nodeY.txt")
    y_node_coords = np.flipud(y_node_coords)
    y_coords = (y_node_coords[1:] + y_node_coords[:-1]) / 2

    arias = []
    dt = time[1] - time[0]
    for i in range(len(accs[0])):
        asig = eqsig.AccSignal(accs[:, i], dt)
        arias.append(eqsig.im.calc_arias_intensity(asig)[-1])
    bf, subplot = plt.subplots()
    subplot.plot(arias, y_node_coords)
    subplot.invert_yaxis()
    plt.show()


if __name__ == '__main__':
    create()


