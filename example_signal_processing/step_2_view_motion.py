import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate


def get_nga_ground_motion(ffp):
    a = open(ffp, 'r')
    b = a.readlines()
    a.close()

    npts = int(b[3].split()[1][:-1])
    mot_dt = b[3].split()[3]
    mot_dt = mot_dt.replace('SEC,', '')
    mot_dt = float(mot_dt)

    acc = []
    for i in range(len(b)):
        if i > 3:
            dat = b[i].split()
            for j in range(len(dat)):
                acc.append(float(dat[j]) * 9.8)

    assert npts == len(acc), ('record length does not match npts!', npts, len(acc))

    acc_values = np.array(acc)
    return acc_values, mot_dt


def create():
    record_filename = 'RSN68_2.AT2'
    acc, dt = get_nga_ground_motion(record_filename)
    time = np.arange(0, len(acc)) * dt
    velocity = scipy.integrate.cumtrapz(acc, dx=dt, initial=0)
    displacement = scipy.integrate.cumtrapz(velocity, dx=dt, initial=0)
    a_ind = np.argmax(np.abs(acc))
    v_ind = np.argmax(np.abs(velocity))
    d_ind = np.argmax(np.abs(displacement))
    bf, sps = plt.subplots(nrows=3)
    sps[0].plot(time, acc, c='k', lw=1)
    sps[0].plot(time[a_ind], acc[a_ind], 'o', c='r')
    sps[1].plot(time, velocity, c='k', lw=1)
    sps[1].plot(time[v_ind], velocity[v_ind], 'o', c='r')
    sps[2].plot(time, displacement, c='k', lw=1)
    sps[2].plot(time[d_ind], displacement[d_ind], 'o', c='r')

    sps[0].set_ylabel("Accel. [$m/s^2$]")
    sps[1].set_ylabel("Velocity [m/s]")
    sps[2].set_ylabel("Disp. [m]")
    sps[-1].set_xlabel("Time [s]")

    plt.show()


if __name__ == '__main__':
    create()
