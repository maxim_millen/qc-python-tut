# Nearly everything in python is namespace, which makes it fast and clean
# - but remember to import what you need
import numpy as np  # numerical library
import datetime


def create():

    # A numpy array must contain data of the same type (all strings, all integers, or all floats, etc)
    strange_array = np.array([0, 3, 5])  # [0, 3, 5]

    # multiplying an array, multiples the individual items
    two_times_array = 2 * strange_array
    assert len(strange_array) == len(two_times_array)
    assert 2 * strange_array[1] == two_times_array[1]

    # You can create 2D or many dimension arrays
    a = []
    for i in range(5):
        a.append(strange_array)
    a = np.array(a)
    print(a.shape)
    assert len(a) == 5
    assert len(a[0]) == 3

    # arrays can be created in many ways
    stepped_array = np.linspace(0.1, 5, 300)  # from 0.1 to 5 in steps of 300
    assert max(stepped_array) == 5

    # arrays are very fast
    equivalent_list = list(stepped_array)

    t0 = datetime.datetime.now()  # start the timer
    a2 = stepped_array ** 2  # perform some operations on the array
    a_again = np.sqrt(a2)
    a_plus_3 = a_again + 3
    sum_a_p3 = np.sum(a_plus_3)
    t1 = datetime.datetime.now()  # stop array timer, start list timer
    sum_list_a_p3 = 0
    for i in range(len(equivalent_list)):  # perform the same operations looping through the list
        a2 = equivalent_list[i] ** 2
        a_again = np.sqrt(a2)
        a_plus_3 = a_again + 3
        sum_list_a_p3 += a_plus_3
    t2 = datetime.datetime.now()  # stop list timer
    array_time = t1 - t0
    list_time = t2 - t1

    assert sum_list_a_p3 == sum_a_p3  # check values are the same

    print(array_time.microseconds, list_time.microseconds)
    assert list_time > array_time


if __name__ == '__main__':
    create()
