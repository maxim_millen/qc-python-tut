import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate
import eqsig


def get_nga_ground_motion(ffp):
    a = open(ffp, 'r')
    b = a.readlines()
    a.close()

    npts = int(b[3].split()[1][:-1])
    mot_dt = b[3].split()[3]
    mot_dt = mot_dt.replace('SEC,', '')
    mot_dt = float(mot_dt)

    acc = []
    for i in range(len(b)):
        if i > 3:
            dat = b[i].split()
            for j in range(len(dat)):
                acc.append(float(dat[j]) * 9.8)

    assert npts == len(acc), ('record length does not match npts!', npts, len(acc))

    acc_values = np.array(acc)
    return acc_values, mot_dt


def load_simple_motion(ffp):
    data = np.genfromtxt(ffp, skip_header=1, delimiter=",", names=True, usecols=0)
    dt = data.dtype.names[0].split("_")[-1]
    dt = "." + dt[1:]
    dt = float(dt)
    values = data.astype(np.float)
    return values, dt


def create(show=0, save=0):
    record_filename = 'RSN68_2.AT2'
    acc, dt = get_nga_ground_motion(record_filename)
    asig = eqsig.AccSignal(acc, dt)
    acc_2, dt_2 = load_simple_motion('test_motion_dt0p01.txt')
    asig2 = eqsig.AccSignal(acc_2, dt_2)

    bf, sps = plt.subplots(nrows=2, squeeze=False, sharex=True)
    sps = sps.flatten()
    response_times = np.linspace(0.01, 4, 30)
    asig.generate_response_spectrum(response_times=response_times, xi=0.05)
    asig2.generate_response_spectrum(response_times=response_times, xi=0.05)

    sps[0].semilogy(1. / asig.fa_frequencies, np.abs(asig.fa_spectrum), c='k', lw=1)
    sps[0].semilogy(1. / asig2.fa_frequencies, np.abs(asig2.fa_spectrum), c='r', lw=1)
    sps[1].plot(response_times, asig.s_a, label='motion 1', c='k')
    sps[1].plot(response_times, asig2.s_a, label="motion 2", c='r')
    sps[1].axvspan(0.6, 1.1, color='b', alpha=0.4, label='building period range')

    sps[0].set_ylabel("FAS [$m/s$]")
    sps[0].set_xlabel("Period [s]")
    sps[1].set_ylabel("Spectral acc. [$m/s^2$]")
    sps[1].set_xlabel("Period [s]")
    sps[1].set_xlim([0, 4])
    sps[1].legend(loc='upper right', prop={'size': 7})

    plt.tight_layout()
    if save:
        bf.savefig('fas_of_motion.png', dpi=100)
    if show:
        plt.show()


if __name__ == '__main__':
    create(save=0, show=1)
