import numpy as np  # numerical library
import matplotlib.pyplot as plt  # plot library


def clean_my_plot(subplot):  # a function that modifies a subplot object
    subplot.tick_params(axis="both", which="both", bottom=False, top=False,
                        labelbottom=False, left=False, right=False, labelleft=True)


def create():
    x = np.linspace(0.1, 15, 30)  # from 0.1 to 5 in steps of 30
    y = 0.1 * x * np.sin(x)

    y2 = y + 0.3 * -x

    y3 = (y2 + 6) ** 2

    bf, sps = plt.subplots(nrows=2)  # create a figure (bf) and 2 subplots (sps)
    # plot x versus y, specify colour, linestyle, line width, marker style, label for legend
    sps[0].plot(x, y, c='r', ls='--', lw=0.7, marker="^", label='wiggle')

    # fill in between two sets of y-values
    sps[0].fill_between(x, y, y2, color='orange', alpha=0.3)  # alpha makes plot transparent
    # set some text at 30% of the width of the plot, and 40% of the height
    sps[0].text(0.3, 0.4, '%s' % "Some text",
                verticalalignment='top', horizontalalignment='left',
                transform=sps[0].transAxes,
                color='black', fontsize=9)
    # define a legend, with font size
    sps[0].legend(prop={'size': 6})
    # set the limits of the x-axis
    sps[0].set_xlim([0, 14])
    # plot with y-axis in log scale
    sps[1].semilogy(x, y3, c=(0.8, 0.4, 0.0))  # set the color using rgb
    # add a black vertical line at x=8
    sps[1].axvline(8, c='k')
    # pass the subplot object to a function to remove the axis ticks
    clean_my_plot(sps[1])
    plt.show()


if __name__ == '__main__':
    create()
