# qc-python-tut

This repository provides some examples for engineering research

- `diffs_w_matlab`: Scripts that demonstrate the key features of python
- `example_signal_processing`: Loading a ground motion, plotting it and performing signal processing
- `example_foundation_design_w_objs`: This example demonstrates how 
objects can be used to write clean reusable code
- `example_opensees_site_response`: This example shows how you can read in a text file and replace the contents
the call opensees.exe from within python
