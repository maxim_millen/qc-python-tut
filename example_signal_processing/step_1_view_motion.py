import numpy as np
import matplotlib.pyplot as plt


def get_nga_ground_motion(ffp):
    a = open(ffp, 'r')
    b = a.readlines()
    a.close()

    npts = int(b[3].split()[1][:-1])
    mot_dt = b[3].split()[3]
    mot_dt = mot_dt.replace('SEC,', '')
    mot_dt = float(mot_dt)

    acc = []
    for i in range(len(b)):
        if i > 3:
            dat = b[i].split()
            for j in range(len(dat)):
                acc.append(float(dat[j]) * 9.8)

    assert npts == len(acc), ('record length does not match npts!', npts, len(acc))

    acc_values = np.array(acc)
    return acc_values, mot_dt


def create():
    record_filename = 'RSN68_2.AT2'
    acc, dt = get_nga_ground_motion(record_filename)
    time = np.arange(0, len(acc)) * dt
    plt.plot(time, acc)
    plt.show()


if __name__ == '__main__':
    create()
