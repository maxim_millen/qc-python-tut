PUB_FIG_DPI = 200
PUB_FIG_FILE_TYPE = ".png"
PUB_DOCUMENT_TYPE = "latex"
ONE_COL = 3.  # inches
TWO_COL = 6.25  # inches
PUB_TRANS = True  # background transparency