

def create():  # functions start with 'def' and have no end, instead the indent determines the end
    print("Hello")  # output is through the print command

    # A list can contain anything
    strange_list = []
    strange_list.append(101)
    strange_list.append('jaguar')
    strange_list.append([3, 4])  # including other lists
    strange_list.append('last item')

    assert strange_list[0] == 101  # python is 0-indexed (first value is accessed using 0)
    assert strange_list[-1] == 'last item'  # last item is accessed using negative 1

    # Python is type sensitive
    try:
        print(strange_list[1.4])  # cannot access list using floats
    except TypeError:
        print("Accessing of the list failed, now I will try using an integer")
        print(strange_list[1])

    # multiplying a list extends the list
    double_list = 2 * strange_list
    assert 2 * len(strange_list) == len(double_list)

    # can access characters in string
    assert 'word'[2] == 'r'
    assert 'word'[1:-1] == 'or'  # can access a range of characters

    a_dictionary = {'a': 100}  # a 'dict' contains unorder key-value pairs
    # Note: if you want them to be ordered, use an OrderedDict from the collections package
    print(a_dictionary['a'])
    assert a_dictionary['a'] == 100
    alphabet = 'abcdefgh'
    for i in range(1, 4):  # counts from 1 to 3, 2nd entry to 4th entry
        a_dictionary[alphabet[i]] = i
    print(a_dictionary)

    # power symbol is '**' not '^'
    assert 6 ** 2 == 36


if __name__ == '__main__':
    create()
