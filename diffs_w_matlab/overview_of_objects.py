

class SimpleObject(object):
    name = 'name for object'  # a class attribute
    special_salt = 3

    def __init__(self, water, earth):  # receives two inputs on initialisation
        self.water = water
        self.earth = earth
        self.fire = self.water + self.earth + self.special_salt  # This is an attribute - not accessed with ()

    def fire_and_water(self):  # this is a method, accessed with ()
        return self.fire, self.water


def create():

    sim_obj = SimpleObject(water=3, earth=4)
    print(sim_obj.fire)
    print(sim_obj.fire_and_water())

    second_obj = SimpleObject(water=6, earth=22)
    print(second_obj.fire)
    assert second_obj.fire == 31


if __name__ == '__main__':
    create()
