import subprocess
import matplotlib.pyplot as plt
import numpy as np
from bwplot import cbox

import eqsig
import sfsimodels as sm
from example_opensees_site_response import op_funs

import json
import os
import platform
import liquepy as lq

# import all_paths as ap

CUR_DIR = os.path.dirname(os.path.abspath(__file__)) + "/"


def call_free_field_single(sp, acc_scale, name, rock_vs):
    sl = sp.layer(1)
    rock = sp.layer(2)
    print(rock.calc_shear_vel(saturated=False))
    assert isinstance(sl, sm.Soil)
    a = open("freeFieldSingle_template.tcl")
    b = a.read()
    b = b.replace("###SOIL_PROFILE_HEIGHT###", str(sp.layer_height(1)))  # opensees code set up for only top layer
    b = b.replace("###ACC_SCALE###", str(acc_scale))
    b = b.replace("###MASS_DENSITY###", str(sl.unit_dry_mass / 1e3))
    b = b.replace("###SHEAR_VELOCITY###", str(sl.calc_shear_vel(saturated=False)))
    b = b.replace("###ROCK_SHEAR_VELOCITY###", str(rock.calc_shear_vel(saturated=False)))
    b = b.replace("###COHESION###", str(sl.cohesion / 1e3))
    b = b.replace("###NAME###", str(name))
    c = open("freeFieldSingle_instance.tcl", 'w')
    c.write(b)
    c.close()
    if platform.system() == 'Darwin':
        p = subprocess.call(["./OpenSees", "freeFieldSingle_instance.tcl"])
    else:
        p = subprocess.call(["OpenSees.exe", "freeFieldSingle_instance.tcl"])


def create(save=0, show=0, cached=1):
    j = 0
    bf, sps = plt.subplots(ncols=2)
    scales = [0.01]

    sps[-1].axvline(0, c="k", ls="--")

    sl = sm.Soil()
    vs = 150.
    vs_rock = 600.  # m/s
    unit_mass = 1700.0
    sl.cohesion = 38.25e3
    sl.g_mod = vs ** 2 * unit_mass
    print('G_mod: ', sl.g_mod)

    sl.unit_dry_weight = unit_mass * 9.8
    assert np.isclose(vs, sl.calc_shear_vel(saturated=False))
    sp = sm.SoilProfile()
    sp.id = 1
    sp.add_layer(0, sl)

    rock = sm.Soil()
    rock.unit_dry_weight = unit_mass * 9.8  # TODO: rock unit weight should be heavier
    rock.g_mod = vs_rock ** 2 * unit_mass
    sp.add_layer(30.0, rock)
    sp.height = 31.0
    sp.set_soil_ids_to_layers()
    ecp_out = sm.Output()
    ecp_out.add_to_dict(sp)
    ofile = open('ecp.json', 'w')
    ofile.write(json.dumps(ecp_out.to_dict(), indent=4))
    ofile.close()

    rock_vs = 500.0  # m/s

    for j in range(len(scales)):
        scale = scales[j]
        name = str(scale)
        name = name.replace(".", "p")
        print(scale)

        if not cached:
            call_free_field_single(sp, scale, name, rock_vs)

        y_coords = np.loadtxt("nodeY.txt")
        y_coords = np.flipud(y_coords)
        y_coords = (y_coords[1:] + y_coords[:-1]) / 2

        strains, time = op_funs.get_strains_and_time(folder_path=CUR_DIR, name=name)
        stresses, time = op_funs.get_stresses_and_time(folder_path=CUR_DIR, name=name)
        max_strains = np.max(np.abs(strains), axis=0)

        sps[1].plot(strains[:, 4], stresses[:, 4], c=cbox(j))
        sps[0].plot(max_strains, y_coords, c=cbox(j))

    sps[0].set_ylabel("Depth [m]")
    sps[0].set_xlabel("Max. strain")
    sps[1].set_xlabel("strain")
    sps[1].set_ylabel("Stress [Pa]")
    sps[0].invert_yaxis()
    plt.show()


if __name__ == '__main__':
    create(cached=0)
