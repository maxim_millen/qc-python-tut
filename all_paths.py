import os

ROOT_DIR = os.path.dirname(os.path.abspath(__file__)) + "/"

FIG_FOLDER = "figures"
PUB_FIG_PATH = ROOT_DIR + FIG_FOLDER + "/"
