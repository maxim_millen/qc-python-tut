import numpy as np
import sfsimodels as sm
import pysra

from liquepy.sra import sm_profile_to_pysra


def get_stresses_and_time(folder_path="", name="1"):
    data = np.loadtxt(folder_path + 'stress%s.out' % name)
    time = data[:, 0]
    all_nodes = data[:, 1:] * 1e3  # convert to Pa
    s_xy_ind = 3  # see http://opensees.berkeley.edu/OpenSees/manuals/usermanual/1550.htm
    return np.take(all_nodes, np.arange(s_xy_ind, len(all_nodes[0]), 5), axis=1), time


def get_strains_and_time(folder_path="", name="1"):
    data = np.loadtxt(folder_path + 'strain%s.out' % name)
    time = data[:, 0]
    all_nodes = data[:, 1:]
    g_xy_ind = 2  # see http://opensees.berkeley.edu/OpenSees/manuals/usermanual/1550.htm
    return np.take(all_nodes, np.arange(g_xy_ind, len(all_nodes[0]), 3), axis=1), time


def get_acc_and_time(folder_path="", name="1"):
    data = np.loadtxt(folder_path + 'acceleration%s.out' % name)
    time = data[:, 0]
    all_nodes = data[:, 1:]
    acc_ind = 2  # three nodes per height
    return np.take(all_nodes, np.arange(acc_ind, len(all_nodes[0]), 4), axis=1), time


def get_in_motion_and_dt(folder_path=""):
    data = np.loadtxt(folder_path + "GilroyNo1EW.out")
    dt = 0.005
    acc = data.reshape(data.size) * 9.81  # should be in m/s2
    return acc, dt


def sra_w_pysra(sp, m, odepths, analysis="linear", d_inc=None):
    if d_inc is None:
        d_inc = np.ones(sp.n_layers)

    profile = sm_profile_to_pysra(sp, d_inc=d_inc)
    if analysis == "linear":
        calc = pysra.propagation.LinearElasticCalculator()
    else:
        calc = pysra.propagation.EquivalentLinearCalculator()
    od = {}
    outs = []
    for i, depth in enumerate(odepths):
        od["upACCX_d%i" % i] = len(outs)
        outs.append(pysra.output.AccelerationTSOutput(pysra.output.OutputLocation('incoming_only', depth=depth)))
        od["ACCX_d%i" % i] = len(outs)
        outs.append(pysra.output.AccelerationTSOutput(pysra.output.OutputLocation('within', depth=depth)))
        od["STRS_d%i" % i] = len(outs)
        outs.append(pysra.output.StrainTSOutput(pysra.output.OutputLocation('within', depth=depth), in_percent=False))
        od["TAU_d%i" % i] = len(outs)
        outs.append(pysra.output.StressTSOutput(pysra.output.OutputLocation('within', depth=depth),
                                                normalized=False))

    outputs = pysra.output.OutputCollection(*outs)

    # Perform the calculation
    calc(m, profile, profile.location('outcrop', depth=sp.height))
    outputs(calc)

    out_series = {}
    for item in od:
        if "TAU" in item:
            out_series[item] = outputs[od[item]].values
        else:
            out_series[item] = outputs[od[item]].values

    return out_series, profile, calc
